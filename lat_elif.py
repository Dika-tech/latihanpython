a = 4
b = 3

if a < b:
  print("Hello")

elif a > b:
  print("World")

else:
  print("!")

a = "ABC"
b = "abc"
c = "ABC"
d = "CBA"

if a == b:
    print("123")
elif a == c:
    print("456")

if a == b:
    print("1234")
else:
    print("789")

a = "Hello"
b = "Hey"

if a == b:
    print("ABC")
else:
    print("DEF")

a = "ABC"
b = "abc"
c = "ABC"
d = "CBA"

if a == b:
    print("123")
if c == d:
    print("456")
else:
    print("789")