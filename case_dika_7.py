print("Kalkulator BMI")
tinggi = float(input("Masukkan tinggi anda (m): "))
berat = float(input("Masukkan berat anda (kg): "))
bmi = berat / (tinggi * tinggi)
hbmi = "{:.2f}".format(bmi)
status = ""
for i in range(1):
    if bmi < 17.0 :
        status = "Kurus"
    elif bmi >= 17.0 and bmi < 18.5 :
        status = "Kurus"
    elif bmi >= 18.5 and bmi <= 25 :
        status = "Normal"
    elif bmi >= 25.1 and bmi <= 27 :
        status = "Gemuk"
    elif bmi > 27 :
        status = "Gemuk"
print("BMI Anda " + hbmi + ", "+status)